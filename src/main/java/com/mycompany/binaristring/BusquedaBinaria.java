package com.mycompany.binaristring;

import java.util.Scanner;
/**
 *
 * @author ameri
 */
public class BusquedaBinaria {

    public static void main(String[] args) {
        String[] arregloA = {"Ake", "Balan", "Castañeda", "Gregorio", "Huicab", "Rodriguez",};

        String[] arregloN = {"Angel", "Benito", "Carolina", "Eduardo", "Fernando", "Karla"};

        String[] arregloT = {"913-123-55-77", "981-453-44-88", "913-432-22-33", "981-567-33-45", "913-233-66-99"};

        Scanner entrada = new Scanner(System.in);
        String busqueda="";
        
        System.out.println("Ingrese el apellido: ");
        busqueda = entrada.nextLine();

        int indiceDelElementoBuscado = 0;
        int numeroTelefonico;

        indiceDelElementoBuscado = busquedaBinaria(arregloA, busqueda);
        numeroTelefonico = indiceDelElementoBuscado;

        System.out.println("Nombre: " + arregloN[numeroTelefonico]);
        System.out.println("Apellido: " + arregloA[numeroTelefonico]);
        System.out.println("Numero Telefonico: " + arregloT[numeroTelefonico]);

    }

    public static int busquedaBinaria(String[] arreglo, String busqueda) {

        int izquierda = 0, derecha = arreglo.length - 1;

        while (izquierda <= derecha) {
            // Calculamos las mitades
            int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
            String elementoDelMedio = arreglo[indiceDelElementoDelMedio];

            // Primero vamos a comparar y ver si el resultado es negativo, positivo o 0
            int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);

            // Si el resultado de la comparación es 0, significa que ambos elementos son iguales
            // y por lo tanto quiere decir que hemos encontrado la búsqueda
            if (resultadoDeLaComparacion == 0) {
                return indiceDelElementoDelMedio;
            }

            // Si no, entonces vemos si está a la izquierda o derecha
            if (resultadoDeLaComparacion < 0) {
                derecha = indiceDelElementoDelMedio - 1;
            } else {
                izquierda = indiceDelElementoDelMedio + 1;
            }
        }
        // Si no se rompió el ciclo ni se regresó el índice, entonces el elemento no
        // existe
        return -1;

    }

}
